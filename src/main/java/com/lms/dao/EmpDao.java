package com.lms.dao;

import java.util.List;

import com.lms.entity.Employee;
import com.lms.exceptions.EmployeeNotFoundException;
import com.lms.exceptions.EmployeeNotSavedException;

public interface EmpDao {

	public List<Employee> getAllEmployee()throws EmployeeNotFoundException;

	public Employee getEmployeeDetailsById(int id)throws EmployeeNotFoundException;

	public Employee getEmployeeInProbation(int id)throws EmployeeNotFoundException;

	public Employee getEmployeeLeavesByName(String name)throws EmployeeNotFoundException;

	public Employee getEmployeePosition(String name)throws EmployeeNotFoundException;

	public boolean saveEmployeeDetails(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)throws EmployeeNotSavedException ;

	public Employee updateEmployeeDetailsById(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)throws EmployeeNotFoundException;
}
