package com.lms.service;

import java.util.List;

import org.springframework.aop.ThrowsAdvice;

import com.lms.dao.EmpDaoImpl;
import com.lms.entity.Employee;
import com.lms.exceptions.EmployeeNotFoundException;
import com.lms.exceptions.EmployeeNotSavedException;

public class EmployeeServiceImpl implements EmployeeService {

	EmpDaoImpl empDaoImplimentation = new EmpDaoImpl();

	@Override
	public List<Employee> getAllEmployee() throws EmployeeNotFoundException{
		List<Employee> elist = empDaoImplimentation.getAllEmployee();

		return elist;
	}

	@Override
	public Employee getEmployeeDetailsById(int id) throws EmployeeNotFoundException{
		Employee e = empDaoImplimentation.getEmployeeDetailsById(id);

		return e;
	}

	@Override
	public Employee getEmployeeInProbation(int id)throws EmployeeNotFoundException {
		Employee e = empDaoImplimentation.getEmployeeInProbation(id);

		return e;
	}

	@Override
	public Employee getEmployeeLeavesByName(String name)throws EmployeeNotFoundException {
		Employee e = empDaoImplimentation.getEmployeeLeavesByName(name);

		return e;
	}

	@Override
	public Boolean saveEmployeeDetails(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)
			throws EmployeeNotSavedException {
		Boolean flag = empDaoImplimentation.saveEmployeeDetails(id, name, designation, isProbation,
				totalLeavesAvailable, noOfLeavesAvailed, isApprovedLeave);

		if (flag == false) {
			throw new EmployeeNotSavedException("Empployee not saved");

		}

		return flag;
	}

	@Override
	public Employee updateEmployeeDetailsById(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)
			throws EmployeeNotFoundException {
		Employee e = empDaoImplimentation.updateEmployeeDetailsById(id, name, designation, isProbation,
				totalLeavesAvailable, noOfLeavesAvailed, isApprovedLeave);
		return e;
	}

}
